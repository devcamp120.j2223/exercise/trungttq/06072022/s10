import { Grid, Typography, TextField, Button } from "@mui/material";
import { useState } from "react";

function SignUpForm () {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    return (
        <Grid container textAlign="center">
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} >
                <Typography variant="h4" gutterBottom component="div">
                    Sign Up For Free
                </Typography>
            </Grid>
            <Grid container item xs={12} lg={12} md={12} sm={12} mt={3}>
                <Grid item xs={6} lg={6} md={12} sm={12} padding={1}>
                    <TextField label="Firstname *" variant="outlined" fullWidth value={firstname} onChange={(event) => {setFirstname(event.target.value)}}/>
                </Grid>
                <Grid item xs={6} lg={6} md={12} sm={12} padding={1}>
                    <TextField label="Lastname *" variant="outlined" fullWidth value={lastname} onChange={(event) => {setLastname(event.target.value)}}/>
                </Grid>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                <TextField label="Email Address *" variant="outlined" fullWidth value={email} onChange={(event) => {setEmail(event.target.value)}}/>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                <TextField type="password" label="Set A Password *" variant="outlined" value={password} fullWidth onChange={(event) => {setPassword(event.target.value)}}/>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} mb={5} padding={1}>
                <Button color="success" variant="contained" size="large" fullWidth>SIGN UP</Button>
            </Grid>
        </Grid>
    )
}

export default SignUpForm;