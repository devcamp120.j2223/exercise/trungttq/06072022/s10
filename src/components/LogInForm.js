import { Grid, Typography, TextField, Button } from "@mui/material";
import { useState } from "react";

function LogInForm () {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    return (
        <Grid container textAlign="center">
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3}>
                <Typography variant="h4" gutterBottom component="div">
                    Welcome Back!
                </Typography>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
                <TextField label="Email Address *" variant="outlined" fullWidth value={email} onChange={(event) => {setEmail(event.target.value)}}/>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                <TextField type="password" label="Password *" variant="outlined" fullWidth value={password} onChange={(event) => {setPassword(event.target.value)}}/>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3} mb={5} padding={1}>
                <Button color="success" variant="contained" size="large" fullWidth>LOG IN</Button>
            </Grid>
        </Grid>
    )
}

export default LogInForm;