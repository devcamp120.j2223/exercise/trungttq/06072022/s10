import { Button, ButtonGroup, Container, Grid } from "@mui/material";
import { useState } from "react";
import LogInForm from "./components/LogInForm";
import SignUpForm from "./components/SignUpForm";

function App() {
  const [isLogIn, setIsLogIn] = useState(true);

  const logInClick = () => {
    setIsLogIn(true);
  }

  const signUpClick = () => {
    setIsLogIn(false);
  }

  return (
    <Container>
      <Grid container sx={{height: "100vh", justifyContent: "space-evenly", alignItems: "center"}}>
        <Grid item xs={6} lg={6} md={8} sm={12} padding={3} sx={{backgroundColor: "#254a1d24", borderRadius: "3px"}}>
          <ButtonGroup fullWidth variant="contained">
            <Button color={isLogIn ? "success" : "inherit"} onClick={logInClick} size="large">Log In</Button>
            <Button color={!isLogIn ? "success" : "inherit"} onClick={signUpClick} size="large">Sign Up</Button>
          </ButtonGroup>
          {
            isLogIn ? 
              <LogInForm />
              :
              <SignUpForm />
          }
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
